#!/usr/bin/julia

module rpgStuff

# using Base.Threads
# using Distributed # Not found?

# function roller(n::Int,d=6::Int)
# 	Roll = Vector{Int}(undef,n)
# 	@simd for k = 1:n
# 		@inbounds Roll[k] = rand(1:d)
# 	end
# 	return Roll
# end

function roller(n::Int,d=6::Int)
	Roll = Vector{Int}(undef,n)
	@simd for k = 1:n
		@inbounds Roll[k] = rand(1:d)
	end
	return Roll
end

function countOnes(roll::Vector{Int})
	Count = 0
	for R in roll
		if R == 1
			Count += 1
		end
	end
	return Count
end

function explodingOnes(roll::Vector{Int},d=6::Int)
	count = countOnes(roll)
	return roller(count,d)
end

function countErrors(n::Int,savant=false::Bool,d=6::Int)
	Errors = 0
	Ones = countOnes(roller(n,d))
	if savant 
		 Ones -= 1
	end
	Errors = Ones
	while Ones > 0
		Ones = countOnes(roller(Ones,d))
		Errors += Ones
	end
	if Errors <0
		Errors = 0
	end
	return Errors
end

function probabilityEstimater(n::Int,savant=true::Bool,d=6::Int)
	errors = 0.0
	trials = 10^7
	@simd for k = 1:trials
		@inbounds errors += countErrors(n,savant,d)
	end
	return errors/trials
end


# function probabilityEstimater(n::Int,savant=true::Bool,d=6::Int)
# 	trials = 10^7
# 	errors = Distributed.@distributed (+) for k = 1:trials
# 		countErrors(n,savant,d)
# 	end
# 	return errors/trials
# end

function totalCost(c::Int,r::Int,wounds=0::Int,savant=false::Bool)
	for n=1:r
		wounds += countErrors(c+wounds,savant)
	end
	return wounds
end

function totalCost(C::Vector{Int},wounds=0::Int,savant=false::Bool)
	for n in C
		wounds += countErrors(n+wounds,savant)
	end
	return wounds
end

# function woundProb

function averageCost(c::Int,r::Int,wounds=0::Int,savant=false::Bool)
	Trials = 10^7
	totalWounds = 0
	for n=1:Trials
		totalWounds += totalCost(c,r,wounds,savant)
	end
	return totalWounds/Trials
end

function averageCost(C::Vector{Int},wounds=0::Int,savant=false::Bool)
	Trials = 10^7
	totalWounds = 0
	for n=1:Trials
		totalWounds += totalCost(C,wounds,savant)
	end
	return totalWounds/Trials
end

end
