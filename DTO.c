#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_sf_gamma.h>

double upower(int a, unsigned int b){
	double result = 1;
	for (int k = 0; k < b; k++){
		result *= a;
	}
	return result;
}

int main(int argc, char *argv[]){
	long double S = 0;
	unsigned int n = atoi(argv[1]);
	if(argc >2){
		printf("Arguments after the first (%d) will be ignored\n",n);
	}

	if(n > 2){
		for (unsigned int q = 0; q <= n - 3; q++){
			S += gsl_sf_choose(n,q)*upower(4,q)*(upower(2,n-q)+q-n-2);
		}
	}
	printf(
		//"Chance of being able to make us of Danger to others is:\n %Lf%%\n"
		"For N=%d:\t%Lf%%\n"
		,n,S/upower(6,n)*100);
	return 0;
}
