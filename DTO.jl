#!/usr/bin/julia

module DTO

function chance(n::Int64)
	S = 0
	if n > 2
		for q = 0:(n-3)
			S += summand(n,q)
		end
	end
	println("The Chance of Danger to Other's triggering is:")
	println(S/6.0^n*100,"%")
end

function summand(n::Int64,q::Int64)
	return binomial(n,q)*4^q*(2^(n-q)+q-n-2)
end

end
